package resolution.example6.zzeulki.practice71;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment1 extends Fragment {


    public BlankFragment1() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_blank_fragment1, container, false);
        ListView listView = (ListView) v.findViewById(R.id.listView);

        ArrayList<String> arr = new ArrayList<String>();
        arr.add("강슬기");
        arr.add("KNU");
        arr.add("박아무개");
        arr.add("김아무개");

        ArrayAdapter<String> name = new ArrayAdapter<String>(
                this.getContext(),
                R.layout.item,
                R.id.name,
                arr
        );

        listView.setAdapter(name);

        return v;
    }

}
